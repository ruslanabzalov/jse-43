package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.SessionDTO;

import java.util.List;

public interface IProjectEndpoint {

    long sizeProjects(@Nullable SessionDTO session);

    boolean isEmptyProjectList(@Nullable SessionDTO session);

    void createProject(@Nullable SessionDTO session, @NotNull String name, @NotNull String description);

    void addAllProjects(@Nullable SessionDTO session, @Nullable List<ProjectDTO> projects);

    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable SessionDTO session);

    @Nullable
    ProjectDTO findProjectsById(@Nullable SessionDTO session, @Nullable Long id);

    void clearAllProjects(@Nullable SessionDTO session);

    long projectsSize(@Nullable SessionDTO session);

    boolean areProjectsEmpty(@Nullable SessionDTO session);

    @NotNull
    List<ProjectDTO> findAllProjectsById(@Nullable SessionDTO session);

    @Nullable
    ProjectDTO findProjectById(@Nullable SessionDTO session, @Nullable Long id);

    @Nullable
    ProjectDTO findProjectByName(@Nullable SessionDTO session, @Nullable String name);

    @Nullable
    ProjectDTO editProjectById(@Nullable SessionDTO session, @Nullable Long id,
                               @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO editProjectByName(@Nullable SessionDTO session, @Nullable String name,
                                 @Nullable String description);

    void clearProjects(@Nullable SessionDTO session);

    void removeProjectById(@Nullable SessionDTO session, @Nullable Long id);

    void removeProjectByName(@Nullable SessionDTO session, @Nullable String name);

    @Nullable
    ProjectDTO startProjectById(@Nullable SessionDTO session, @Nullable Long id);

    @Nullable
    ProjectDTO endProjectById(@Nullable SessionDTO session, @Nullable Long id);

    @NotNull
    List<ProjectDTO> sortProjectsByName(@Nullable SessionDTO session);

    @NotNull
    List<ProjectDTO> sortProjectsByStartDate(@Nullable SessionDTO session);

    @NotNull
    List<ProjectDTO> sortProjectsByEndDate(@Nullable SessionDTO session);

    @NotNull
    List<ProjectDTO> sortProjectsByStatus(@Nullable SessionDTO session);
    
}
