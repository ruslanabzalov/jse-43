package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.ProjectDTO;

import java.sql.Timestamp;
import java.util.List;

public interface IProjectRepository {

    long projectsSize();

    long projectsSizeByUserId(@NotNull Long userId);

    void createProject(@NotNull ProjectDTO projectDTO);

    void clearAllProjects();

    void clearProjectByUserId(@NotNull Long userId);

    @Nullable
    List<ProjectDTO> findAllProjects();

    @Nullable
    List<ProjectDTO> findProjectsByUserId(@NotNull Long userId);

    @Nullable
    ProjectDTO findProjectById(@NotNull Long id);

    @Nullable
    ProjectDTO findProjectByName(@NotNull Long userId, @NotNull String name);

    void removeProjectById(@NotNull Long id);

    void removeProjectByName(@NotNull Long userId, @NotNull String name);

    void editProjectById(@NotNull Long id, @NotNull String name, @NotNull String description);

    void editProjectByName(@NotNull Long userId, @NotNull String name, @NotNull String description);

    void startProjectById(@NotNull Long id, @NotNull Timestamp startDate, @NotNull String status);

    void endProjectById(@NotNull Long id, @NotNull Timestamp endDate, @NotNull String status);

    @Nullable
    List<ProjectDTO> sortProjectsByName(@NotNull Long userId);

    @Nullable
    List<ProjectDTO> sortProjectsByStartDate(@NotNull Long userId);

    @Nullable
    List<ProjectDTO> sortProjectsByEndDate(@NotNull Long userId);

    @Nullable
    List<ProjectDTO> sortProjectsByStatus(@NotNull Long userId);

}
