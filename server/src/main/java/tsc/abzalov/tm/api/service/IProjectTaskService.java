package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskService {

    boolean hasData(@Nullable Long userId);

    void addTaskToProjectById(@Nullable Long taskId, @Nullable Long projectId);

    @NotNull
    ProjectDTO findProjectById(@Nullable Long id);

    @NotNull
    TaskDTO findTaskById(@Nullable Long id);

    @NotNull
    List<TaskDTO> findProjectTasksById(@Nullable Long userId, @Nullable Long projectId);

    void deleteProjectById(@Nullable Long id);

    void deleteProjectTasksById(@Nullable Long userId, @Nullable Long projectId);

}
