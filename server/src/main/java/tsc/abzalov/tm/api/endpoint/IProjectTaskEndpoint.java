package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskEndpoint {

    boolean hasData(@Nullable SessionDTO session);

    void addTaskToProjectById(@Nullable SessionDTO session, @Nullable Long projectId,
                              @Nullable Long taskId);

    @Nullable
    ProjectDTO findTaskProjectById(@Nullable SessionDTO session, @Nullable Long id);

    @Nullable
    TaskDTO findProjectTaskById(@Nullable SessionDTO session, @Nullable Long id);

    @NotNull
    List<TaskDTO> findProjectTasksById(@Nullable SessionDTO session, @Nullable Long projectId);

    void deleteProjectById(@Nullable SessionDTO session, @Nullable Long id);

    void deleteProjectTasksById(@Nullable SessionDTO session, @Nullable Long projectId);

}
