package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IFileBackupService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.ISessionService;

public interface IEndpointLocator extends IServiceLocator {

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IFileBackupService getFileBackupService();

}
