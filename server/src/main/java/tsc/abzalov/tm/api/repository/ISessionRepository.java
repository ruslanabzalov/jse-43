package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.SessionDTO;

public interface ISessionRepository {

    void addSession(@NotNull SessionDTO sessionDTO);

    void removeSession(@NotNull final Long id);

    @Nullable
    SessionDTO findSession(@NotNull final Long id);

}
