package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepository {
    
    long usersSize();
    
    void createUser(@NotNull UserDTO userDTO);
    
    void clearAllUsers();

    @NotNull
    List<UserDTO> findAllUsers();

    @Nullable
    UserDTO findUserById(@NotNull Long id);

    @Nullable
    UserDTO findUserByLogin(@NotNull String login);

    @Nullable
    UserDTO findUserByEmail(@NotNull String email);
    
    void removeUserById(@NotNull Long id);
    
    void removeUserByLogin(@NotNull String login);
    
    void editUserPassword(@NotNull Long id, @NotNull String hashedPassword);
    
    void editUserInfo(@NotNull Long id, @NotNull String firstName, @Nullable String lastName);

    void lockUnlockUserById(@NotNull Long id, boolean lockedFlag);
    
    void lockUnlockUserByLogin(@NotNull String login, boolean lockedFlag);

}
