package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskEndpoint {

    long sizeTasks(@Nullable SessionDTO session);

    boolean isEmptyTaskList(@Nullable SessionDTO session);

    void createTask(@Nullable SessionDTO session, @NotNull String name, @NotNull String description);

    void addAllTasks(@Nullable SessionDTO session, @Nullable List<TaskDTO> tasks);

    @Nullable
    TaskDTO findTasksById(@Nullable SessionDTO session, @Nullable Long id);

    void clearAllTasks(@Nullable SessionDTO session);

    long tasksSize(@Nullable SessionDTO session);

    boolean areTasksEmpty(@Nullable SessionDTO session);

    @NotNull
    List<TaskDTO> findAllTasks(@Nullable SessionDTO session);

    @Nullable
    TaskDTO findTaskById(@Nullable SessionDTO session, @Nullable Long id);

    @Nullable
    TaskDTO findTaskByName(@Nullable SessionDTO session, @Nullable String name);

    @Nullable
    TaskDTO editTaskById(@Nullable SessionDTO session, @Nullable Long id,
                         @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO editTaskByName(@Nullable SessionDTO session, @Nullable String name, @Nullable String description);

    void clearTasks(@Nullable SessionDTO session);

    void removeTaskById(@Nullable SessionDTO session, @Nullable Long id);

    void removeTaskByName(@Nullable SessionDTO session, @Nullable String name);

    @Nullable
    TaskDTO startTaskById(@Nullable SessionDTO session, @Nullable Long id);

    @Nullable
    TaskDTO endTaskById(@Nullable SessionDTO session, @Nullable Long id);

    @NotNull
    List<TaskDTO> sortTasksByName(@Nullable SessionDTO session);

    @NotNull
    List<TaskDTO> sortTasksByStartDate(@Nullable SessionDTO session);

    @NotNull
    List<TaskDTO> sortTasksByEndDate(@Nullable SessionDTO session);

    @NotNull
    List<TaskDTO> sortTasksByStatus(@Nullable SessionDTO session);

}
