package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.TaskDTO;

import java.sql.Timestamp;
import java.util.List;

public interface ITaskRepository {

    long tasksSize();

    long tasksSizeByUserId(@NotNull Long userId);

    void createTask(@NotNull TaskDTO taskDTO);

    void clearAllTasks();

    void clearTaskByUserId(@NotNull Long userId);

    void clearTasksByUserId(@NotNull Long userId);

    @Nullable
    List<TaskDTO> findAllTasks();

    @Nullable
    List<TaskDTO> findTasksByUserId(@NotNull Long userId);

    @Nullable
    TaskDTO findTaskById(@NotNull Long id);

    @Nullable
    TaskDTO findTaskByName(@NotNull Long userId, @NotNull String name);

    void removeTaskById(@NotNull Long id);

    void removeTaskByName(@NotNull Long userId, @NotNull String name);

    void editTaskById(@NotNull Long id, @NotNull String name, @NotNull String description);

    void editTaskByName(@NotNull Long userId, @NotNull String name, @NotNull String description);

    void startTaskById(@NotNull Long id, @NotNull Timestamp startDate, @NotNull String status);

    void endTaskById(@NotNull Long id, @NotNull Timestamp endDate, @NotNull String status);

    @Nullable
    List<TaskDTO> sortTasksByName(@NotNull Long userId);

    @Nullable
    List<TaskDTO> sortTasksByStartDate(@NotNull Long userId);

    @Nullable
    List<TaskDTO> sortTasksByEndDate(@NotNull Long userId);

    @Nullable
    List<TaskDTO> sortTasksByStatus(@NotNull Long userId);


    void addTaskToProjectById(@NotNull Long id, @NotNull Long projectId);

    @Nullable
    List<TaskDTO> findProjectTasksById(@NotNull Long userId, @NotNull Long projectId);

    void deleteProjectTasksById(@NotNull Long userId, @NotNull Long projectId);

}
