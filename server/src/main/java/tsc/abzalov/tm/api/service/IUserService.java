package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.dto.UserDTO;

public interface IUserService extends IService<UserDTO> {

    void create(@Nullable String login, @Nullable String password, @Nullable Role role,
                @Nullable String firstName, @Nullable String lastName, @Nullable String email);

    void create(@Nullable String login, @Nullable String password,@Nullable String firstName,
                @Nullable String lastName, @Nullable String email);

    boolean isUserExist(@Nullable String login, @Nullable String email);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO editPasswordById(@Nullable Long id, @Nullable String newPassword);

    @Nullable
    UserDTO editUserInfoById(@Nullable Long id, @Nullable String firstName, @Nullable String lastName);

    void deleteByLogin(@Nullable String login);

    @Nullable
    UserDTO lockUnlockById(@Nullable Long id);

    @Nullable
    UserDTO lockUnlockByLogin(@Nullable String login);

}
