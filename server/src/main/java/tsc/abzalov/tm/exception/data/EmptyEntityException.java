package tsc.abzalov.tm.exception.data;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyEntityException extends AbstractException {

    public EmptyEntityException() {
        super("Entity is empty!");
    }

}
