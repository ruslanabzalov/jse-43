package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Status;

import javax.persistence.*;
import java.time.LocalDateTime;

import static tsc.abzalov.tm.enumeration.Status.TODO;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @Column
    @Nullable
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = TODO;

    @Nullable
    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Nullable
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Nullable
    @ManyToOne
    private User user;
}
