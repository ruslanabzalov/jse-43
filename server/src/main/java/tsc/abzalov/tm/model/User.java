package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static tsc.abzalov.tm.enumeration.Role.USER;

@Data
@Entity
@Table(name = "user")
@EqualsAndHashCode(callSuper = true)
public final class User extends AbstractEntity {

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = USER;

    @Column
    @Nullable
    private String firstname;

    @Column
    @Nullable
    private String lastname;

    @Column
    @Nullable
    private String email;

    @Column(name = "locked_flag")
    private boolean lockedFlag = false;

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();
}
