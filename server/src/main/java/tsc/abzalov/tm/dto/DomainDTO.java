package tsc.abzalov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.TaskDTO;
import tsc.abzalov.tm.dto.UserDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import static tsc.abzalov.tm.util.Formatter.DATE_TIME_FORMATTER;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class DomainDTO implements Serializable {

    @Nullable
    private List<ProjectDTO> projects;

    @Nullable
    private List<TaskDTO> tasks;

    @Nullable
    private List<UserDTO> users;

    @NotNull
    private String backupDateTime = LocalDateTime.now().format(DATE_TIME_FORMATTER);

}
