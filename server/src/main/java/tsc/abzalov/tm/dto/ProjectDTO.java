package tsc.abzalov.tm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "project")
@EqualsAndHashCode(callSuper = true)
public class ProjectDTO extends AbstractBusinessEntityDTO implements Serializable {
}
