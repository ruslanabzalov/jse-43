package tsc.abzalov.tm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Data
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractEntityDTO implements Serializable {

    @Id
    @NotNull
    private Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;

}
