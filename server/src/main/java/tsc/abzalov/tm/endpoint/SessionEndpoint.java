package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.ISessionEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public SessionDTO openSession(@WebParam(name = "login") @Nullable String login,
                                  @WebParam(name = "password") @Nullable String password) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        return sessionService.openSession(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.closeSession(session);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public SessionDTO findSession(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        return sessionService.findSession(session);
    }

}
