package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.dto.TaskDTO;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public long tasksSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM TaskDTO e", Long.class)
                .getSingleResult();
    }

    @Override
    public long tasksSizeByUserId(@NotNull Long userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM TaskDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void createTask(@NotNull TaskDTO taskDTO) {
        entityManager.persist(taskDTO);
    }

    @Override
    public void clearAllTasks() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    public void clearTaskByUserId(@NotNull Long userId) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clearTasksByUserId(@NotNull Long userId) {
        clearTaskByUserId(userId);
    }

    @Override
    public @Nullable List<TaskDTO> findAllTasks() {
        return entityManager.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class).getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findTasksByUserId(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findTaskById(@NotNull Long id) {
        @NotNull val result = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.id = :id", TaskDTO.class)
                .setParameter("id", id)
                .getResultList();
        return (result.isEmpty()) ? null : result.get(0);
    }

    @Override
    public @Nullable TaskDTO findTaskByName(@NotNull Long userId, @NotNull String name) {
        @NotNull val result =
                entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                        .setParameter("userId", userId)
                        .setParameter("name", name)
                        .getResultList();
        return (result.isEmpty()) ? null : result.get(0);
    }

    @Override
    public void removeTaskById(@NotNull Long id) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.id = :id", TaskDTO.class)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeTaskByName(@NotNull Long userId, @NotNull String name) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void editTaskById(@NotNull Long id, @NotNull String name, @NotNull String description) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.name = :name, e.description = :description WHERE e.id = :id", TaskDTO.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void editTaskByName(@NotNull Long userId, @NotNull String name, @NotNull String description) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.description = :description WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void startTaskById(@NotNull Long id, @NotNull Timestamp startDate, @NotNull String status) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.startDate = :startDate, e.status = :status WHERE e.id = :id", TaskDTO.class)
                .setParameter("startDate", startDate)
                .setParameter("status", status)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void endTaskById(@NotNull Long id, @NotNull Timestamp endDate, @NotNull String status) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.endDate = :endDate, e.status = :status WHERE e.id = :id", TaskDTO.class)
                .setParameter("endDate", endDate)
                .setParameter("status", status)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public @Nullable List<TaskDTO> sortTasksByName(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId ORDER BY e.name DESC", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> sortTasksByStartDate(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId ORDER BY e.startDate DESC", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> sortTasksByEndDate(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId ORDER BY e.endDate DESC", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> sortTasksByStatus(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId ORDER BY e.status DESC", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void addTaskToProjectById(@NotNull Long id, @NotNull Long projectId) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.projectId = :projectId WHERE e.id = :id", TaskDTO.class)
                .setParameter("projectId", projectId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public @Nullable List<TaskDTO> findProjectTasksById(@NotNull Long userId, @NotNull Long projectId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void deleteProjectTasksById(@NotNull Long userId, @NotNull Long projectId) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }
}
