package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ISessionRepository;
import tsc.abzalov.tm.dto.SessionDTO;

import javax.persistence.EntityManager;

public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void addSession(@NotNull SessionDTO sessionDTO) {
        entityManager.persist(sessionDTO);
    }

    @Override
    public void removeSession(@NotNull Long id) {
        entityManager.createQuery("DELETE FROM SessionDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public @Nullable SessionDTO findSession(@NotNull Long id) {
        @NotNull val result =
                entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.id = :id", SessionDTO.class)
                        .setParameter("id", id)
                        .getResultList();
        return (result.isEmpty() ? null : result.get(0));
    }
}
