package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.dto.ProjectDTO;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public long projectsSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class)
                .getSingleResult();
    }

    @Override
    public long projectsSizeByUserId(@NotNull Long userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void createProject(@NotNull ProjectDTO projectDTO) {
        entityManager.persist(projectDTO);
    }

    @Override
    public void clearAllProjects() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    public void clearProjectByUserId(@NotNull Long userId) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @Nullable List<ProjectDTO> findAllProjects() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class).getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> findProjectsByUserId(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable ProjectDTO findProjectById(@NotNull Long id) {
        @NotNull val result = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .getResultList();
        return (result.isEmpty()) ? null : result.get(0);
    }

    @Override
    public @Nullable ProjectDTO findProjectByName(@NotNull Long userId, @NotNull String name) {
        @NotNull val result =
                entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultList();
        return (result.isEmpty()) ? null : result.get(0);
    }

    @Override
    public void removeProjectById(@NotNull Long id) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeProjectByName(@NotNull Long userId, @NotNull String name) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void editProjectById(@NotNull Long id, @NotNull String name, @NotNull String description) {
        entityManager.createQuery("UPDATE ProjectDTO e SET e.name = :name, e.description = :description WHERE e.id = :id", ProjectDTO.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void editProjectByName(@NotNull Long userId, @NotNull String name, @NotNull String description) {
        entityManager.createQuery("UPDATE ProjectDTO e SET e.description = :description WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void startProjectById(@NotNull Long id, @NotNull Timestamp startDate, @NotNull String status) {
        entityManager.createQuery("UPDATE ProjectDTO e SET e.startDate = :startDate, e.status = :status WHERE e.id = :id", ProjectDTO.class)
                .setParameter("startDate", startDate)
                .setParameter("status", status)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void endProjectById(@NotNull Long id, @NotNull Timestamp endDate, @NotNull String status) {
        entityManager.createQuery("UPDATE ProjectDTO e SET e.endDate = :endDate, e.status = :status WHERE e.id = :id", ProjectDTO.class)
                .setParameter("endDate", endDate)
                .setParameter("status", status)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public @Nullable List<ProjectDTO> sortProjectsByName(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId ORDER BY e.name DESC", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> sortProjectsByStartDate(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId ORDER BY e.startDate DESC", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> sortProjectsByEndDate(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId ORDER BY e.endDate DESC", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> sortProjectsByStatus(@NotNull Long userId) {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId ORDER BY e.status DESC", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }
}
