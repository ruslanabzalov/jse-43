package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public long usersSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class).getSingleResult();
    }

    @Override
    public void createUser(@NotNull UserDTO userDTO) {
        entityManager.persist(userDTO);
    }

    @Override
    public void clearAllUsers() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    public @NotNull List<UserDTO> findAllUsers() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", UserDTO.class).getResultList();
    }

    @Override
    public @Nullable UserDTO findUserById(@NotNull Long id) {
        @NotNull val result =
                entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                        .setParameter("id", id)
                        .getResultList();

        return (result.isEmpty() ? null : result.get(0));
    }

    @Override
    public @Nullable UserDTO findUserByLogin(@NotNull String login) {
        @NotNull val result =
                entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                        .setParameter("login", login)
                        .getResultList();

        return (result.isEmpty() ? null : result.get(0));
    }

    @Override
    public @Nullable UserDTO findUserByEmail(@NotNull String email) {
        @NotNull val result =
                entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.email = :email", UserDTO.class)
                        .setParameter("email", email)
                        .getResultList();

        return (result.isEmpty() ? null : result.get(0));
    }

    @Override
    public void removeUserById(@NotNull Long id) {
        entityManager.createQuery("DELETE FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeUserByLogin(@NotNull String login) {
        entityManager.createQuery("DELETE FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void editUserPassword(@NotNull Long id, @NotNull String hashedPassword) {
        entityManager.createQuery("UPDATE UserDTO e SET e.password = :password WHERE e.id = :id", UserDTO.class)
                .setParameter("password", hashedPassword)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void editUserInfo(@NotNull Long id, @NotNull String firstName, @Nullable String lastName) {
        entityManager.createQuery("UPDATE UserDTO e SET e.firstname = :firstname, e.lastname = :lastname WHERE e.id = :id", UserDTO.class)
                .setParameter("firstname", firstName)
                .setParameter("lastname", lastName)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void lockUnlockUserById(@NotNull Long id, boolean lockedFlag) {
        entityManager.createQuery("UPDATE UserDTO e SET e.lockedFlag = :lockedFlag WHERE e.id = :id", UserDTO.class)
                .setParameter("lockedFlag", lockedFlag)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void lockUnlockUserByLogin(@NotNull String login, boolean lockedFlag) {
        entityManager.createQuery("UPDATE UserDTO e SET e.lockedFlag = :lockedFlag WHERE e.login = :login", UserDTO.class)
                .setParameter("lockedFlag", lockedFlag)
                .setParameter("login", login)
                .executeUpdate();
    }
}
