package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.ISessionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.auth.SessionIsInactiveException;
import tsc.abzalov.tm.exception.auth.UserLockedException;
import tsc.abzalov.tm.repository.SessionRepository;

import java.time.LocalDateTime;

import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.HashUtil.hash;
import static tsc.abzalov.tm.util.SessionUtil.signSession;

public final class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IHashingPropertyService propertyService;

    public SessionService(@NotNull final IConnectionService connectionService,
                          @NotNull final IUserService userService,
                          @NotNull final IHashingPropertyService propertyService) {
        this.connectionService = connectionService;
        this.userService = userService;
        this.propertyService = propertyService;
    }


    @Override
    @NotNull
    @SneakyThrows
    public SessionDTO openSession(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) throw new IncorrectCredentialsException();
        @Nullable val user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLockedFlag()) throw new UserLockedException();

        @NotNull val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        if (user.getPassword() == null) throw new IncorrectCredentialsException();
        if (!user.getPassword().equals(hash(password, counter, salt))) throw new AccessDeniedException();

        @NotNull val session = new SessionDTO();
        session.setUserId(user.getId());
        session.setOpenDate(LocalDateTime.now());

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val sessionRepository = new SessionRepository(entityManager);

            @NotNull val id = session.getId();
            @Nullable val openDate = session.getOpenDate();
            @Nullable val userId = session.getUserId();

            if (openDate == null || userId == null) throw new AccessDeniedException();

            @NotNull val sessionDTO = new SessionDTO();
            sessionDTO.setId(id);
            sessionDTO.setOpenDate(openDate);
            sessionDTO.setUserId(userId);

            transaction.begin();
            sessionRepository.addSession(sessionDTO);
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }

        @Nullable val signedSession = sign(session);
        if (signedSession == null) throw new AccessDeniedException();
        return signedSession;
    }

    @Override
    @SneakyThrows
    public void closeSession(@Nullable final SessionDTO session) {
        if (session == null) throw new SessionIsInactiveException();

        session.setSignature(null);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val sessionRepository = new SessionRepository(entityManager);
            transaction.begin();
            sessionRepository.removeSession(session.getId());
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getOpenDate() == null) throw new AccessDeniedException();
        userService.findById(session.getUserId());

        @Nullable var tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();

        tempSession.setSignature(null);
        tempSession = sign(tempSession);

        if (tempSession == null) throw new AccessDeniedException();
        if (tempSession.getSignature() == null) throw new AccessDeniedException();
        if (!tempSession.getSignature().equals(session.getSignature())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdminPermissions(@Nullable SessionDTO session, @NotNull IUserService userService) {
        validate(session);

        if (session.getUserId() != null) {
            @Nullable val user = userService.findById(session.getUserId());
            if (user == null) throw new AccessDeniedException();
            if (!user.getRole().equals(ADMIN)) throw new AccessDeniedException();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public SessionDTO findSession(@Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getOpenDate() == null) throw new AccessDeniedException();

        @Nullable val oldSign = session.getSignature();
        session.setSignature(null);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val sessionRepository = new SessionRepository(entityManager);
            @Nullable var foundedSession = sessionRepository.findSession(session.getId());

            if (foundedSession == null) throw new AccessDeniedException();

            session.setSignature(oldSign);
            foundedSession = sign(foundedSession);

            if (foundedSession == null) throw new AccessDeniedException();
            if (!foundedSession.equals(session)) throw new AccessDeniedException();
            return foundedSession;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @SneakyThrows
    private SessionDTO sign(@NotNull final SessionDTO session) {
        @NotNull val sessionCounter = propertyService.getSessionCounterProperty();
        @NotNull val sessionSalt = propertyService.getSessionSaltProperty();

        return signSession(session, sessionCounter, sessionSalt);
    }

}
