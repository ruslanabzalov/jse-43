package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.TaskDTO;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public boolean hasData(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val projectRepository = new ProjectRepository(entityManager);
            @NotNull val taskRepository = new TaskRepository(entityManager);
            return projectRepository.projectsSizeByUserId(userId) != 0 && taskRepository.tasksSizeByUserId(userId) != 0;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addTaskToProjectById(@Nullable Long taskId, @Nullable Long projectId) {
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        taskId = Optional.ofNullable(taskId).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            @NotNull val taskRepository = new TaskRepository(entityManager);
            taskRepository.addTaskToProjectById(taskId, projectId);
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public ProjectDTO findProjectById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val projectRepository = new ProjectRepository(entityManager);
            @Nullable val searchedProject = projectRepository.findProjectById(id);
            return Optional.ofNullable(searchedProject).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDTO findTaskById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try  {
            @NotNull val taskRepository = new TaskRepository(entityManager);
            @Nullable val searchedTask = taskRepository.findTaskById(id);
            return Optional.ofNullable(searchedTask).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDTO> findProjectTasksById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val taskRepository = new TaskRepository(entityManager);
            return Optional
                    .ofNullable(taskRepository.findProjectTasksById(userId, projectId))
                    .orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void deleteProjectById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            @NotNull val projectRepository = new ProjectRepository(entityManager);
            projectRepository.removeProjectById(id);
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void deleteProjectTasksById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val taskRepository = new TaskRepository(entityManager);
            transaction.begin();
            taskRepository.deleteProjectTasksById(userId, projectId);
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

}
