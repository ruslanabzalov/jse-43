package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.dto.AbstractBusinessEntityDTO;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.TaskDTO;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Status.DONE;
import static tsc.abzalov.tm.enumeration.Status.IN_PROGRESS;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;

public abstract class AbstractBusinessEntityService<T extends AbstractBusinessEntityDTO>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IConnectionService connectionService;

    public AbstractBusinessEntityService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public long size() {
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return projectRepository.projectsSize();
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return taskRepository.tasksSize();
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final T entity) {
        if (entity == null) throw new EntityNotFoundException();

        @NotNull val id = entity.getId();
        @Nullable val name = entity.getName();
        @Nullable var description = entity.getDescription();
        @Nullable val startDate = (entity.getStartDate() == null)
                ? null
                : Timestamp.valueOf(entity.getStartDate());
        @Nullable val endDate = (entity.getEndDate() == null)
                ? null
                : Timestamp.valueOf(entity.getEndDate());
        @NotNull val status = entity.getStatus();
        @Nullable val userId = entity.getUserId();

        if (name == null) throw new EmptyNameException();
        if (description == null) description = DEFAULT_DESCRIPTION;
        if (userId == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                @NotNull val project = new ProjectDTO();
                project.setId(id);
                project.setName(name);
                project.setDescription(description);
                project.setStartDate((startDate == null) ? null : startDate.toLocalDateTime());
                project.setEndDate((endDate == null) ? null : endDate.toLocalDateTime());
                project.setStatus(status);
                project.setUserId(userId);

                projectRepository.createProject(project);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);

                @NotNull val task = (TaskDTO) entity;
                @Nullable val projectId = task.getProjectId();

                @NotNull val taskDTO = new TaskDTO();
                taskDTO.setId(id);
                taskDTO.setName(name);
                taskDTO.setDescription(description);
                taskDTO.setStartDate((startDate == null) ? null : startDate.toLocalDateTime());
                taskDTO.setEndDate((endDate == null) ? null : endDate.toLocalDateTime());
                taskDTO.setStatus(status);
                taskDTO.setUserId(userId);
                taskDTO.setProjectId(projectId);
                taskRepository.createTask(taskDTO);
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<T> entities) {
        if (entities == null) throw new EmptyEntityException();
        entities.forEach(this::create);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<T> findAll() {
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(projectRepository.findAllProjects())
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(taskRepository.findAllTasks())
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public T findById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (T) Optional
                        .ofNullable(projectRepository.findProjectById(id))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (T) Optional
                        .ofNullable(taskRepository.findTaskById(id))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.clearAllProjects();
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.clearAllTasks();
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.removeProjectById(id);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.removeTaskById(id);
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long size(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return projectRepository.projectsSizeByUserId(userId);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return taskRepository.tasksSizeByUserId(userId);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        return this.size(userId) == 0;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(projectRepository.findProjectsByUserId(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(taskRepository.findTasksByUserId(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public T findByName(@Nullable Long userId, @Nullable String name) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (T) Optional
                        .ofNullable(projectRepository.findProjectByName(userId, name))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (T) Optional
                        .ofNullable(taskRepository.findTaskByName(userId, name))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public T editById(@Nullable final Long id, @Nullable final String name,
                      @Nullable final String description) {
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val correctDescription = (description == null) ? DEFAULT_DESCRIPTION : description;

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.editProjectById(id, name, correctDescription);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.editTaskById(id, name, correctDescription);
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }

        return this.findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public T editByName(@Nullable final Long userId, @Nullable final String name,
                        @Nullable final String description) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val correctDescription = (description == null) ? DEFAULT_DESCRIPTION : description;

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.editProjectByName(userId, name, correctDescription);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.editTaskByName(userId, name, correctDescription);
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }

        return this.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final Long userId) {
        if (userId == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.clearProjectByUserId(userId);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.clearTasksByUserId(userId);
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.removeProjectByName(userId, name);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.removeTaskByName(userId, name);
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public T startById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.startProjectById(id, Timestamp.valueOf(LocalDateTime.now()), IN_PROGRESS.toString());
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.startTaskById(id, Timestamp.valueOf(LocalDateTime.now()), IN_PROGRESS.toString());
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }

        return this.findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public T endById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                projectRepository.endProjectById(id, Timestamp.valueOf(LocalDateTime.now()), IN_PROGRESS.toString());
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                taskRepository.endTaskById(id, Timestamp.valueOf(LocalDateTime.now()), DONE.toString());
            }
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            exception.printStackTrace();
        } finally {
            entityManager.close();
        }

        return this.findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByName(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByName(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByName(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStartDate(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByStartDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByStartDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByEndDate(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByEndDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByEndDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStatus(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = new ProjectRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByStatus(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = new TaskRepository(entityManager);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByStatus(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        } finally {
            entityManager.close();
        }
    }

}
