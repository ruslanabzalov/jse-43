package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.dto.UserDTO;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.EmptyEmailException;
import tsc.abzalov.tm.exception.auth.EmptyFirstNameException;
import tsc.abzalov.tm.exception.auth.EmptyLoginException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IHashingPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IHashingPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    public long size() {
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            transaction.begin();
            val result = userRepository.usersSize();
            transaction.commit();
            return result;
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final UserDTO user) {
        if (user == null) throw new EntityNotFoundException();

        @NotNull val id = user.getId();
        @Nullable val login = user.getLogin();
        @Nullable val password = user.getPassword();
        @NotNull val role = user.getRole();
        @Nullable val firstname = user.getFirstname();
        @Nullable val lastname = user.getLastname();
        @Nullable val email = user.getEmail();
        boolean isLocked = user.isLockedFlag();

        checkMainInfo(login, password, firstname, email);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);

            @NotNull val userDTO = new UserDTO();
            userDTO.setId(id);
            userDTO.setLogin(login);
            userDTO.setPassword(password);
            userDTO.setRole(role);
            userDTO.setFirstname(firstname);
            userDTO.setLastname(lastname);
            userDTO.setEmail(email);
            userDTO.setLockedFlag(isLocked);

            transaction.begin();
            userRepository.createUser(user);
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, role, firstName, lastName, email));
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, USER, firstName, lastName, email));
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<UserDTO> users) {
        if (users == null) throw new EntityNotFoundException();
        for (@NotNull val user : users) this.create(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            return userRepository.findAllUsers();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            transaction.begin();
            userRepository.clearAllUsers();
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            transaction.begin();
            userRepository.removeUserById(id);
            transaction.commit();
        } catch (@NotNull final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            return Optional.ofNullable(userRepository.findUserById(id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isUserExist(@Nullable String login, @Nullable String email) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        email = Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);

            val isExistByLogin = userRepository.findUserByLogin(login) != null;
            val isExistByEmail = userRepository.findUserByEmail(email) != null;

            return isExistByLogin && isExistByEmail;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            @Nullable val searchedUser = userRepository.findUserByLogin(login);
            return Optional.ofNullable(searchedUser).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO editPasswordById(@Nullable Long id, @Nullable String newPassword) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        newPassword = Optional.ofNullable(newPassword).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @Nullable UserDTO user;
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            transaction.begin();
            userRepository.editUserPassword(id, hash(newPassword, counter, salt));
            transaction.commit();
            user = userRepository.findUserById(id);
        } catch (Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }

        return Optional.ofNullable(user).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO editUserInfoById(@Nullable Long id, @Nullable String firstName, @Nullable final String lastName) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        firstName = Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);

        @Nullable UserDTO user;
        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        try {
            @NotNull val userRepository = new UserRepository(entityManager);
            transaction.begin();
            userRepository.editUserInfo(id, firstName, lastName);
            transaction.commit();
            user = userRepository.findUserById(id);
        } catch (Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }

        return Optional.ofNullable(user).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void deleteByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        @NotNull val userRepository = new UserRepository(entityManager);
        try {
            transaction.begin();
            userRepository.removeUserByLogin(login);
            transaction.commit();
        } catch (Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO lockUnlockById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        @NotNull val userRepository = new UserRepository(entityManager);

        @NotNull val user = Optional
                .ofNullable(userRepository.findUserById(id))
                .orElseThrow(EntityNotFoundException::new);
        boolean neededFlag = user.isLockedFlag();

        @Nullable UserDTO changedUser;
        try {
            transaction.begin();
            userRepository.lockUnlockUserById(id, neededFlag);
            transaction.commit();
            changedUser = userRepository.findUserById(id);
        } catch (Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }

        return Optional
                .ofNullable(changedUser)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO lockUnlockByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val entityManager = connectionService.getEntityManagerFactory().createEntityManager();
        @NotNull val transaction = entityManager.getTransaction();
        @NotNull val userRepository = new UserRepository(entityManager);

        @NotNull val user = Optional.ofNullable(userRepository.findUserByLogin(login)).orElseThrow(EntityNotFoundException::new);
        boolean neededFlag = !user.isLockedFlag();

        @Nullable UserDTO changedUser;
        try {
            transaction.begin();
            userRepository.lockUnlockUserByLogin(login, neededFlag);
            transaction.commit();
            changedUser = userRepository.findUserByLogin(login);
        } catch (Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }

        return Optional
                .ofNullable(changedUser)
                .orElseThrow(EntityNotFoundException::new);
    }

    @SneakyThrows
    private void checkMainInfo(@Nullable String login, @Nullable String password,
                               @Nullable String firstName, @Nullable String email) {
        if (login == null) throw new IncorrectCredentialsException();
        if (password == null) throw new IncorrectCredentialsException();
        if (firstName == null) throw new EmptyFirstNameException();
        if (email == null) throw new EmptyEmailException();
    }

    @NotNull
    @SneakyThrows
    private UserDTO createUser(@Nullable String login, @Nullable String password,
                               @Nullable Role role, @Nullable String firstName,
                               @Nullable String lastName, @Nullable String email) {
        checkMainInfo(login, password, firstName, email);

        @NotNull val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @NotNull val user = new UserDTO();
        user.setLogin(login);
        user.setPassword(hash(password, counter, salt));
        if (role != null) user.setRole(role);
        else user.setRole(USER);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setEmail(email);
        return user;
    }

}
