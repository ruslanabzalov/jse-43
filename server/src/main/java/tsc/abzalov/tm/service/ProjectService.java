package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.dto.ProjectDTO;

public final class ProjectService extends AbstractBusinessEntityService<ProjectDTO> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
