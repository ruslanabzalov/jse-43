package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IApplicationPropertyService extends IDeveloperInfoPropertyService,
        IHashingPropertyService, IServerPropertyService {

    void initLocalProperties();

    @NotNull
    String getAppVersion();

}
