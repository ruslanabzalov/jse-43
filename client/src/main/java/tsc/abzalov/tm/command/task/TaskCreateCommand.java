package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;


public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "create-task";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create task.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("TASK CREATION");
        @NotNull val name = inputName();
        @NotNull val description = inputDescription();

        @NotNull val taskEndpoint = getServiceLocator().getTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        taskEndpoint.createTask(session, name, description);

        System.out.println("Tasks was successfully created.\n");
    }

}
