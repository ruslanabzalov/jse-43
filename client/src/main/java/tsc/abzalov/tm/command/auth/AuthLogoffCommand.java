package tsc.abzalov.tm.command.auth;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;


public final class AuthLogoffCommand extends AbstractCommand {

    public AuthLogoffCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "logoff";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Logoff from existing user.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("LOGOFF");
        @NotNull val sessionEndpoint = getServiceLocator().getSessionEndpoint();
        @NotNull val session = getServiceLocator().getSession();
        sessionEndpoint.closeSession(session);
        getServiceLocator().setSession(null);
        System.out.println("Successful logoff.\n");
    }

}
