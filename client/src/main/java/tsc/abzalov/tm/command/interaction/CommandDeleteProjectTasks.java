package tsc.abzalov.tm.command.interaction;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;


public final class CommandDeleteProjectTasks extends AbstractCommand {

    public CommandDeleteProjectTasks(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "delete-project-with-tasks";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Delete project with tasks.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("DELETE PROJECTS WITH TASKS");
        @NotNull val projectTaskEndpoint = getServiceLocator().getProjectTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();
        val isDataExist = projectTaskEndpoint.hasData(session);
        if (isDataExist) {
            System.out.println("Project");
            @NotNull val projectId = InputUtil.inputId();
            System.out.println();

            @Nullable val searchedProject =
                    projectTaskEndpoint.findTaskProjectById(session, projectId);
            val isProjectNotExist = !Optional.ofNullable(searchedProject).isPresent();
            if (isProjectNotExist) {
                System.out.println("Project was not found.\n");
                return;
            }

            projectTaskEndpoint.deleteProjectTasksById(session, projectId);
            projectTaskEndpoint.deleteProjectById(session, projectId);
            System.out.println("Project and it tasks was successfully deleted.\n");
            return;
        }

        System.out.println("One of the lists is empty!\n");
    }

}
