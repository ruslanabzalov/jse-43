package tsc.abzalov.tm.endpoint;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static tsc.abzalov.tm.endpoint.Status.DONE;
import static tsc.abzalov.tm.endpoint.Status.IN_PROGRESS;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;

class ProjectEndpointTest {

    @NotNull
    private static final String LOGIN = "admin";

    @NotNull
    private static final String PASSWORD = "admin";

    @NotNull
    private static final String PROJECT_NAME = "Testing Project Name";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "Testing Project Description";

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private Session session;

    @Nullable
    private Project project;

    @BeforeEach
    void setUp() {
        this.session = this.sessionEndpoint.openSession(LOGIN, PASSWORD);
        projectEndpoint.createProject(this.session, PROJECT_NAME, PROJECT_DESCRIPTION);
        project = projectEndpoint.findProjectByName(session, PROJECT_NAME);
    }

    @AfterEach
    void tearDown() {
        projectEndpoint.clearProjects(session);
        if (this.session != null) this.sessionEndpoint.closeSession(session);
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Project By Id Test")
    void editProjectById() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectById(null, this.project.getId(), PROJECT_NAME, PROJECT_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectById(this.session, null, PROJECT_NAME, PROJECT_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectById(this.session, this.project.getId(), null, PROJECT_DESCRIPTION)
                ),
                () -> {
                    @Nullable val editedProject =
                            projectEndpoint.editProjectById(this.session, this.project.getId(), PROJECT_NAME + PROJECT_NAME, null);
                    assertNotNull(editedProject);
                    assertEquals(PROJECT_NAME + PROJECT_NAME, editedProject.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Project Index Test")
    void projectIndex() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.projectIndex(null, this.project)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.projectIndex(this.session, null)
                ),
                () -> {
                    val projectCorrectIndex =
                            projectEndpoint.sizeProjects(session) > 0 ? projectEndpoint.sizeProjects(session) - 1 : 0;
                    val projectIndex = projectEndpoint.projectIndex(this.session, this.project);
                    assertEquals(projectCorrectIndex, projectIndex);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Project By Id Test")
    void findProjectsById() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectsById(null, this.project.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectsById(this.session, null)
                ),
                () -> {
                    @Nullable val foundedProject =
                            projectEndpoint.findProjectsById(this.session, this.project.getId());
                    assertNotNull(foundedProject);
                    assertEquals(this.project.getName(), foundedProject.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find All Projects Test")
    void findAllProjects() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findAllProjects(null)
                ),
                () -> {
                    @NotNull val projects = projectEndpoint.findAllProjects(this.session);
                    assertNotNull(projects);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Empty Project List Test")
    void isEmptyProjectList() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.isEmptyProjectList(null)
                ),
                () -> {
                    assertFalse(projectEndpoint.isEmptyProjectList(session));
                    projectEndpoint.clearAllProjects(session);
                    assertTrue(projectEndpoint.isEmptyProjectList(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Remove Project By Index Test")
    void removeProjectByIndex() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.removeProjectByIndex(null, 0)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.removeProjectByIndex(this.session, -1)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            val projectIndex = projectEndpoint.projectIndex(this.session, this.project) + 1;
                            projectEndpoint.removeProjectByIndex(this.session, projectIndex);
                            projectEndpoint.findProjectById(this.session, this.project.getId());
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Project By Name Test")
    void findProjectByName() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectByName(null, PROJECT_NAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectByName(this.session, null)
                ),
                () -> {
                    @Nullable val foundedProject =
                            projectEndpoint.findProjectByName(this.session, PROJECT_NAME);
                    assertNotNull(foundedProject);
                    assertEquals(this.project.getDescription(), foundedProject.getDescription());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Project By Name Test")
    void editProjectByName() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectByName(null, PROJECT_NAME, PROJECT_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectByName(this.session, null, PROJECT_DESCRIPTION)
                ),
                () -> {
                    @Nullable val editedProject =
                            projectEndpoint.editProjectByName(this.session, PROJECT_NAME, null);
                    assertNotNull(editedProject);
                    assertEquals(DEFAULT_DESCRIPTION, editedProject.getDescription());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Clear All Projects Test")
    void clearAllProjects() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.clearAllProjects(null)
                ),
                () -> {
                    projectEndpoint.clearAllProjects(this.session);
                    assertTrue(projectEndpoint.isEmptyProjectList(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("End Project By Id Test")
    void endProjectById() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.endProjectById(null, this.project.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.endProjectById(this.session, null)
                ),
                () -> {
                    projectEndpoint.startProjectById(this.session, this.project.getId());
                    @Nullable val endedProject =
                            projectEndpoint.endProjectById(this.session, this.project.getId());
                    assertNotNull(endedProject);
                    assertEquals(DONE, endedProject.getStatus());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Add All Projects Test")
    void addAllProjects() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.addAllProjects(null, new ArrayList<>())
                ),
                () -> {
                    projectEndpoint.addAllProjects(this.session, null);
                    assertTrue(projectEndpoint.isEmptyProjectList(this.session)); // TODO: Обратить внимание.
                },
                () -> {
                    @NotNull val projects = new ArrayList<Project>();
                    for (int i = 0; i < 10; i++) {
                        @NotNull val project = new Project();
                        project.setUserId(this.session.getUserId());
                        projects.add(project);
                    }

                    projectEndpoint.addAllProjects(this.session, projects);
                    assertEquals(projects.size(), projectEndpoint.projectsSize(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Project By Id Test")
    void findProjectById() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectById(null, this.project.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectById(this.session, null)
                ),
                () -> {
                    @Nullable val foundedProject =
                            projectEndpoint.findProjectById(this.session, this.project.getId());
                    assertNotNull(foundedProject);
                    assertEquals(this.project.getName(), foundedProject.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Clear Projects Test")
    void clearProjects() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.clearProjects(null)
                ),
                () -> {
                    projectEndpoint.clearProjects(this.session);
                    assertTrue(projectEndpoint.areProjectsEmpty(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Start Project By Id Test")
    void startProjectById() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.startProjectById(null, this.project.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.startProjectById(this.session, null)
                ),
                () -> {
                    @Nullable val startedProject =
                            projectEndpoint.startProjectById(this.session, this.project.getId());
                    assertNotNull(startedProject);
                    assertEquals(IN_PROGRESS, startedProject.getStatus());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Project By Index Test")
    void findProjectByIndex() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectByIndex(null, 0)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.findProjectByIndex(this.session, -1)
                ),
                () -> {
                    val projectIndex = projectEndpoint.projectIndex(this.session, this.project) + 1;
                    projectEndpoint.findProjectByIndex(this.session, projectIndex);
                    @NotNull val foundedProject = projectEndpoint.findProjectById(this.session, this.project.getId());
                    assertEquals(this.project.getName(), foundedProject.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Remove Project By Id Test")
    void removeProjectById() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.removeProjectById(null, this.project.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.removeProjectById(this.session, null)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            projectEndpoint.removeProjectById(this.session, this.project.getId());
                            projectEndpoint.findProjectById(this.session, this.project.getId());
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Projects Size Test")
    void projectsSize() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.projectsSize(null)
                ),
                () -> {
                    @NotNull val projects = new ArrayList<Project>();
                    for (int i = 0; i < 10; i++) {
                        @NotNull val project = new Project();
                        project.setUserId(this.session.getUserId());
                        projects.add(project);
                    }

                    projectEndpoint.addAllProjects(this.session, projects);
                    assertEquals(projects.size(), projectEndpoint.projectsSize(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Create Project Test")
    void createProject() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.createProject(null, PROJECT_NAME, PROJECT_DESCRIPTION)
                ),
                () -> {
                    projectEndpoint.createProject(this.session, PROJECT_NAME + 1, PROJECT_DESCRIPTION + 1); // TODO: Обратить внимание.
                    assertEquals(2, projectEndpoint.projectsSize(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Projects Empty Test")
    void areProjectsEmpty() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.areProjectsEmpty(null)
                ),
                () -> {
                    assertFalse(projectEndpoint.areProjectsEmpty(this.session));
                    projectEndpoint.removeProjectById(this.session, this.project.getId());
                    assertTrue(projectEndpoint.areProjectsEmpty(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Remove Project By Name Test")
    void removeProjectByName() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.removeProjectByName(null, PROJECT_NAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.removeProjectByName(this.session, null)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            projectEndpoint.removeProjectByName(this.session, PROJECT_NAME);
                            projectEndpoint.findProjectById(this.session, this.project.getId());
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Project By Index Test")
    void editProjectByIndex() {
        assertNotNull(this.project);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectByIndex(null, 1, PROJECT_NAME, PROJECT_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectByIndex(this.session, 0, PROJECT_NAME, PROJECT_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.editProjectByIndex(this.session, 1, null, PROJECT_DESCRIPTION)
                ),
                () -> {
                    @Nullable val editedProject =
                            projectEndpoint.editProjectByIndex(this.session, 1, PROJECT_NAME + 1, null);
                    assertNotNull(editedProject);
                    assertEquals(DEFAULT_DESCRIPTION, editedProject.getDescription());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Size Projects")
    void sizeProjects() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectEndpoint.sizeProjects(null)
                ),
                () -> {
                    @NotNull val projects = new ArrayList<Project>();
                    for (int i = 0; i < 10; i++) {
                        @NotNull val project = new Project();
                        project.setUserId(this.session.getUserId());
                        projects.add(project);
                    }

                    projectEndpoint.addAllProjects(this.session, projects);
                    assertEquals(projects.size(), projectEndpoint.sizeProjects(this.session));
                }
        );
    }

}