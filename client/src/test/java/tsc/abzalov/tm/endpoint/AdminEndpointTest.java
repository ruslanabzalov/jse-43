package tsc.abzalov.tm.endpoint;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static tsc.abzalov.tm.endpoint.Role.USER;

class AdminEndpointTest {

    @NotNull
    private static final String TEST_USER_LOGIN = "test";

    @NotNull
    private static final String LOGIN = "admin";

    @NotNull
    private static final String PASSWORD = "admin";

    @NotNull
    private static final String FIRSTNAME = "Admin";

    @NotNull
    private static final String LASTNAME = "Admin";

    @NotNull
    private static final String EMAIL = "admin@mail.com";

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @Nullable
    private Session session;

    @BeforeEach
    void setUp() {
        session = sessionEndpoint.openSession(LOGIN, PASSWORD);
    }

    @AfterEach
    void tearDown() {
        if (session != null) sessionEndpoint.closeSession(session);
    }

    @Test
    @Tag("Integration")
    @DisplayName("Lock/Unlock User By Id Test")
    void adminLockUnlockUserById() {
        @NotNull val newUser = new User();
        newUser.setLogin(TEST_USER_LOGIN);
        adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
        @Nullable val testUser = adminEndpoint.adminFindUserByLogin(this.session, TEST_USER_LOGIN);
        assertNotNull(testUser);
        @NotNull val testUserId = testUser.getId();
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminLockUnlockUserById(null, testUserId)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminLockUnlockUserById(this.session, null)
                ),
                () -> {
                    val oldLockUnlockStatus = testUser.isLocked();
                    @Nullable val lockUnlockUser = adminEndpoint.adminLockUnlockUserById(this.session, testUserId);
                    assertNotNull(lockUnlockUser);
                    val newLockUnlockStatus = lockUnlockUser.isLocked();
                    assertNotEquals(oldLockUnlockStatus, newLockUnlockStatus);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Create User With Custom Role Test")
    void adminCreateUserWithCustomRole() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithCustomRole(null, LOGIN, PASSWORD, USER, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithCustomRole(this.session, null, PASSWORD, USER, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithCustomRole(this.session, LOGIN, null, USER, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithCustomRole(this.session, LOGIN, PASSWORD, USER, null, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithCustomRole(this.session, LOGIN, PASSWORD, USER, FIRSTNAME, LASTNAME, null)
                ),
                () -> {
                    adminEndpoint.adminCreateUserWithCustomRole(this.session, LOGIN + LOGIN, PASSWORD, USER, FIRSTNAME, LASTNAME, EMAIL);
                    @Nullable val newUser = adminEndpoint.adminFindUserByLogin(this.session, LOGIN + LOGIN);
                    assertNotNull(newUser);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Create User With Entity Test")
    void adminCreateUserWithEntity() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithEntity(null, new User())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUserWithEntity(this.session, null)
                ),
                () -> {
                    @NotNull val newUser = new User();
                    newUser.setLogin(LOGIN + 2);
                    adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
                    assertNotNull(adminEndpoint.adminFindUserByLogin(this.session, LOGIN + 2));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Lock/Unlock User By Login Test")
    void adminLockUnlockUserByLogin() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminLockUnlockUserByLogin(null, TEST_USER_LOGIN)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminLockUnlockUserByLogin(this.session, null)
                ),
                () -> {
                    @NotNull val newUser = new User();
                    newUser.setLogin(TEST_USER_LOGIN);
                    adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
                    @Nullable val testUser = adminEndpoint.adminFindUserByLogin(this.session, TEST_USER_LOGIN);
                    assertNotNull(testUser);
                    val oldLockUnlockStatus = testUser.isLocked();
                    @Nullable val lockUnlockUser = adminEndpoint.adminLockUnlockUserByLogin(this.session, TEST_USER_LOGIN);
                    assertNotNull(lockUnlockUser);
                    val newLockUnlockStatus = lockUnlockUser.isLocked();
                    assertNotEquals(oldLockUnlockStatus, newLockUnlockStatus);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Delete User By Login Test")
    void adminDeleteUserByLogin() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminDeleteUserByLogin(null, LOGIN)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminDeleteUserByLogin(this.session, null)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            @NotNull val newUser = new User();
                            newUser.setLogin(LOGIN + 2);
                            adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
                            adminEndpoint.adminDeleteUserByLogin(this.session, LOGIN + 2);
                            adminEndpoint.adminFindUserByLogin(this.session, LOGIN + 2);
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Size Users Test")
    void adminSizeUsers() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminSizeUsers(null)
                ),
                () -> {
                    @NotNull val newUser = new User();
                    newUser.setLogin(LOGIN + 2);
                    adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
                    assertTrue(adminEndpoint.adminSizeUsers(this.session) > 0);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Add All Users Test")
    void adminAddAllUsers() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminAddAllUsers(null, new ArrayList<>())
                ),
                () -> {
                    @NotNull val users = new ArrayList<User>();
                    val counter = 10;
                    for (int i = 0; i < counter; i++) users.add(adminEndpoint.adminFindUserByLogin(this.session, LOGIN));

                    adminEndpoint.adminAddAllUsers(this.session, users);
                    assertEquals(counter, adminEndpoint.adminSizeUsers(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find User By Id Test")
    void adminFindUsersById() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindUsersById(null, this.session.getUserId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindUsersById(this.session, null)
                ),
                () -> assertNotNull(adminEndpoint.adminFindUsersById(this.session, this.session.getUserId()))
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find All User Test")
    void adminFindAllUsers() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindAllUsers(null)
                ),
                () -> {
                    @NotNull val newUser = new User();
                    newUser.setLogin(LOGIN + 2);
                    adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
                    @NotNull val users = adminEndpoint.adminFindAllUsers(this.session);
                    assertNotNull(users);
                    assertFalse(users.isEmpty());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("User Exist Test")
    void adminIsUserExist() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminIsUserExist(null, LOGIN, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminIsUserExist(this.session, null, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminIsUserExist(this.session, LOGIN, null)
                ),
                () -> assertTrue(adminEndpoint.adminIsUserExist(this.session, LOGIN, EMAIL))
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit User Info By Id Test")
    void adminEditUserInfoById() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminEditUserInfoById(null, this.session.getUserId(), FIRSTNAME, LASTNAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminEditUserInfoById(this.session, null, FIRSTNAME, LASTNAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminEditUserInfoById(null, this.session.getUserId(), null, LASTNAME)
                ),
                () -> {
                    @Nullable val changedUser = adminEndpoint.adminEditUserInfoById(this.session, this.session.getUserId(), FIRSTNAME + 1, null);
                    assertNotNull(changedUser);
                    assertEquals(FIRSTNAME + 1, changedUser.getFirstName());
                    adminEndpoint.adminEditUserInfoById(this.session, this.session.getUserId(), FIRSTNAME, LASTNAME);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Password By Id Test")
    void adminEditPasswordById() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminEditPasswordById(null, this.session.getUserId(), PASSWORD)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminEditPasswordById(this.session, null, PASSWORD)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminEditPasswordById(null, this.session.getUserId(), null)
                ),
                () -> {
                    @Nullable val changedUser = adminEndpoint.adminEditPasswordById(this.session, this.session.getUserId(), PASSWORD);
                    assertNotNull(changedUser);
                    adminEndpoint.adminEditPasswordById(this.session, this.session.getUserId(), PASSWORD);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Empty User List Test")
    void adminIsEmptyUserList() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminIsEmptyUserList(null)
                ),
                () -> {
                    @NotNull val newUser = new User();
                    newUser.setLogin(LOGIN + 2);
                    adminEndpoint.adminCreateUserWithEntity(this.session, newUser);
                    assertFalse(adminEndpoint.adminIsEmptyUserList(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find User By Id Test")
    void adminFindUserUserById() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindUserUserById(null, this.session.getUserId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindUserUserById(this.session, null)
                ),
                () -> assertNotNull(adminEndpoint.adminFindUserUserById(this.session, this.session.getUserId()))
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Create User Test")
    void adminCreateUser() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUser(null, LOGIN, PASSWORD, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUser(this.session, null, PASSWORD, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUser(this.session, LOGIN, null, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUser(this.session, LOGIN, PASSWORD, null, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminCreateUser(this.session, LOGIN, PASSWORD, FIRSTNAME, LASTNAME, null)
                ),
                () -> {
                    adminEndpoint.adminCreateUser(this.session, LOGIN + 1, PASSWORD, FIRSTNAME, LASTNAME, EMAIL);
                    @Nullable val newUser = adminEndpoint.adminFindUserByLogin(this.session, LOGIN + 1);
                    assertNotNull(newUser);
                    assertEquals(LOGIN + 1, newUser.getLogin());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find User By Login Test")
    void adminFindUserByLogin() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindUserByLogin(null, LOGIN)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> adminEndpoint.adminFindUserByLogin(this.session, null)
                ),
                () -> assertNotNull(adminEndpoint.adminFindUserByLogin(this.session, LOGIN))
        );
    }

}